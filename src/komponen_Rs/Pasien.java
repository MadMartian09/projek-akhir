package komponen_Rs;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.swing.JOptionPane;

public class Pasien {

    private String nama;
    private String no_Rm;
    private String jenis_Kelamin;
    private String alamat;
    private String nik;
    private String tanggalLahir;
    private Dokter dok;
    private String status;
    private String tanggalMasuk;
    private String tanggalKeluar;
    private String id_kamar;
    koneksi kon = new koneksi();

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNo_Rm() {
        return no_Rm;
    }

    public void setNo_Rm(String no_Rm) {
        this.no_Rm = no_Rm;
    }

    public String getJenis_Kelamin() {
        return jenis_Kelamin;
    }

    public void setJenis_Kelamin(String jenis_Kelamin) {
        this.jenis_Kelamin = jenis_Kelamin;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public Dokter getDok() {
        return dok;
    }

    public void setDok(Dokter dok) {
        this.dok = dok;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(String tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTanggalMasuk() {
        return tanggalMasuk;
    }

    public void setTanggalMasuk(String tanggalMasuk) {
        this.tanggalMasuk = tanggalMasuk;
    }

    public String getTanggalKeluar() {
        return tanggalKeluar;
    }

    public void setTanggalKeluar(String tanggalKeluar) {
        this.tanggalKeluar = tanggalKeluar;
    }

    public String getId_kamar() {
        return id_kamar;
    }

    public void setId_kamar(String id_kamar) {
        this.id_kamar = id_kamar;
    }

    public void inputData() {
        Date tg = Date.valueOf(getTanggalLahir());
        Connection conn = null;
        String id = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        PreparedStatement ps1 = null;
        conn = this.kon.getKoneksi();

        try {
            ps = conn.prepareStatement("select id_dokter from dokter where nama=?");
            ps.setString(1, dok.getNama_Dok());
            rs = ps.executeQuery();
            while (rs.next()) {
                dok.setId(rs.getString(1));
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
            JOptionPane.showMessageDialog(null, "Error getDok");
        }
        id = dok.getId();
        try {
            ps = conn.prepareStatement("insert into pasien values(?,?,?,?,?,?)");
            ps.setString(1, getNo_Rm());
            ps.setString(2, getNama());
            ps.setString(3, getNik());
            ps.setString(4, getAlamat());
            ps.setDate(5, tg);
            ps.setString(6, jenis_Kelamin);
            ps.executeUpdate();
            conn.commit();
            
            ps1=conn.prepareStatement("insert into pasien_berobat values(?,?,?,?,?,?)");
            ps1.setString(1, getNo_Rm());
            ps1.setString(2, dok.getId());
            ps1.setTimestamp(3, new java.sql.Timestamp(System.currentTimeMillis()));
            ps1.setString(4, "Rawat Jalan");
            ps1.setDate(5, null);
            ps1.setDate(6, null);
            ps1.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        } finally {
            try {
                ps.close();
                ps1.close();
                rs.close();
                conn.close();
            } catch (SQLException ex) {
                System.out.println("Message : " + ex.getMessage());
            }
        }
    }

    public void inputDataInap() {
        Date tg = Date.valueOf(getTanggalLahir());
        Date tgm = Date.valueOf(getTanggalMasuk());
        Connection conn = null;
        String id = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        PreparedStatement ps1=null;
        conn = this.kon.getKoneksi();
        try {
            ps = conn.prepareStatement("select id_dokter from dokter where nama=?");
            ps.setString(1, dok.getNama_Dok());
            rs = ps.executeQuery();
            while (rs.next()) {
                dok.setId(rs.getString(1));
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
            JOptionPane.showMessageDialog(null, "Metod dokter");
        } finally {
            try {
                conn.close();
                ps.close();
                rs.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }
        id = dok.getId();
        conn = this.kon.getKoneksi();
        try {
            ps = conn.prepareStatement("insert into pasien values(?,?,?,?,?,?)");
            ps.setString(1, getNo_Rm());
            ps.setString(2, getNama());
            ps.setString(3, getNik());
            ps.setString(4, getAlamat());
            ps.setDate(5, tg);
            ps.setString(6, jenis_Kelamin);
            ps.executeUpdate();
            conn.commit();
            
            ps1=conn.prepareStatement("insert into pasien_berobat values(?,?,?,?,?,?)");
            ps1.setString(1, getNo_Rm());
            ps1.setString(2, dok.getId());
            ps1.setTimestamp(3, new java.sql.Timestamp(System.currentTimeMillis()));
            ps1.setString(4, "Rawat Inap");
            ps1.setDate(5, tgm);
            ps1.setDate(6, null);
            ps1.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
            JOptionPane.showMessageDialog(null, "Kelas Pasien");
        } finally {
            try {
                ps.close();
                rs.close();
                conn.close();
            } catch (SQLException ex) {
                System.out.println("Message : " + ex.getMessage());
            }
        }
        
    }

    public void updateData() {
        Connection conn = null;
        PreparedStatement ps = null;
        PreparedStatement inap = null;
        conn = kon.getKoneksi();

        try {
            ps = conn.prepareStatement("update pasien set nama=?," + " nik=?," + " alamat=? "
                    + "where no_rm=?");
            ps.setString(1, nama);
            ps.setString(2, nik);
            ps.setString(3, alamat);
            ps.setString(4, no_Rm);
            ps.executeUpdate();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        } finally {
            try {
                ps.close();
                conn.close();
            } catch (SQLException ex) {
                System.out.println("Message : " + ex.getMessage());
            }
        }

    }

    public void updateDataInap() {
        Connection conn = null;
        PreparedStatement ps = null;
        PreparedStatement inap = null;
        conn = kon.getKoneksi();

        try {
            ps = conn.prepareStatement("update pasien set nama=?," + " nik=?, alamat=? "
                    + "where no_rm=?");
            ps.setString(1, nama);
            ps.setString(2, nik);
            ps.setString(3, alamat);
            ps.setString(4, no_Rm);
            ps.executeUpdate();
            inap=conn.prepareStatement("update pasien_berobat set tanggal_masuk=?, status=? where no_rm=?");
            inap.setTimestamp(1, new java.sql.Timestamp(System.currentTimeMillis()));
            inap.setString(2, "Rawat Inap");
            inap.setString(3, no_Rm);
            inap.executeUpdate();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage()+"Error update");
            
        } finally {
            try {
                ps.close();
                inap.close();
                conn.close();
            } catch (SQLException ex) {
                System.out.println("Message : " + ex.getMessage());
            }
        }

    }

    public void updateKamar(String id, String rm) {
        Connection conn = null;

        ResultSet rs = null;
        conn = this.kon.getKoneksi();
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("update kamar set no_rm=?"
                    + "where id_kamar=?");
            ps.setString(1, rm);
            ps.setString(2, id);
            ps.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            try {
                conn.close();
                ps.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }

    }

    public String status(String rm) {
        Connection conn = null;
        ResultSet rs = null;
        conn = this.kon.getKoneksi();
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("select status from pasien_berobat where no_rm=?");
            ps.setString(1, rm);
            rs = ps.executeQuery();
            while (rs.next()) {
                status = rs.getString(1);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
            System.out.println("Error status pas");
        } finally {
            try {
                conn.close();
                ps.close();
                rs.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }
        return status;
    }

}
