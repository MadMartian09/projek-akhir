package komponen_Rs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class Dokter implements biaya {

    private String nama_Dok;
    private String id;
    private final double TARIF_DOKTER = 35000;
    private Connection conn = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private Statement st = null;
    koneksi kon = new koneksi();

    public String getNama_Dok() {
        return nama_Dok;
    }

    public Dokter() {
    }

    public Dokter(String nama_Dok, String id) {
        this.nama_Dok = nama_Dok;
        this.id = id;
    }

    public void setNama_Dok(String nama_Dok) {
        this.nama_Dok = nama_Dok;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public double getBiaya() {
        return TARIF_DOKTER;
    }

    public void getDetailDokter(String rm) {
        conn = this.kon.getKoneksi();
        try {
            ps = conn.prepareStatement("select d.nama, d.id_dokter from pasien p, dokter d, pasien_berobat pb "
                    + "where pb.id_dokter=d.id_dokter and p.no_rm=? and pb.no_rm=?");
            ps.setString(1, rm);
            ps.setString(2, rm);
            rs = ps.executeQuery();
            while (rs.next()) {
                nama_Dok = rs.getString(1);
                id = rs.getString(2);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
            System.out.println("Error getDok");
        } finally {
            try {
                conn.close();
                ps.close();
                rs.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }
    }

}
