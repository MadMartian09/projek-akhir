/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package komponen_Rs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author ASUS
 */
public class Obat implements biaya {

    private String id_Obat;
    private String nama_Obat;
    private int banyak;
    private int harga;
    koneksi kon = new koneksi();
    private Connection conn = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private Statement st = null;

    public String getId_Obat() {
        return id_Obat;
    }

    public void setId_Obat(String id_Obat) {
        this.id_Obat = id_Obat;
    }

    public String getNama_Obat() {
        return nama_Obat;
    }

    public void setNama_Obat(String nama_Obat) {
        this.nama_Obat = nama_Obat;
    }

    public int getBanyak() {
        return banyak;
    }

    public void setBanyak(int banyak) {
        this.banyak = banyak;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    @Override
    public double getBiaya() {
        return harga * banyak;
    }

    public String getIdObat(String nama) {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        conn = this.kon.getKoneksi();
        try {
            ps = conn.prepareStatement("select id_obat from obat where nama_obat=?");
            ps.setString(1, nama);
            rs = ps.executeQuery();
            while (rs.next()) {
                id_Obat = rs.getString(1);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            try {
                conn.close();
                ps.close();
                rs.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }

        return id_Obat;
    }

    public void insertResepObat(String id_resep, String rm) {
        PreparedStatement rs = null;
        conn = this.kon.getKoneksi();
        try {
            ps = conn.prepareStatement("insert into resep_obat values(?,?,?)");
            ps.setString(1, id_resep);
            ps.setString(2, id_Obat);
            ps.setInt(3, banyak);
            ps.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
            System.out.println("eror insertResepObat");
        } finally {
            try {
                ps.close();
                conn.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }
    }

    public void insertResep(String id_resep, String id_Dok, String rm) {
        PreparedStatement rsep = null;
        conn = this.kon.getKoneksi();

        try {
            rsep = conn.prepareStatement("insert into resep values(?,?,?,?)");
            rsep.setString(1, id_resep);
            rsep.setString(2, id_Dok);
            rsep.setString(3, rm);
            rsep.setTimestamp(4, new java.sql.Timestamp(System.currentTimeMillis()));
            rsep.executeUpdate();
            conn.commit();

        } catch (SQLException e) {
            System.out.println("Eror di obat");
            JOptionPane.showMessageDialog(null, e.getMessage());

        } finally {
            try {
                rsep.close();
                conn.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }
    }

    public int getJumObat(String rm) {
        int jumObat = 0;
        conn = this.kon.getKoneksi();
        try {
            ps = conn.prepareStatement("select count(ro.id_obat) from resep_obat ro "
                    + "join resep r "
                    + "on (r.id_resep=ro.id_resep) "
                    + "where r.no_rm=?");
            ps.setString(1, rm);
            rs = ps.executeQuery();
            while (rs.next()) {
                jumObat = rs.getInt(1);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage() + "  Error jumObat");
        } finally {
            try {
                conn.close();
                ps.close();
                rs.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }
        return jumObat;
    }

    public void getDetailObat(String rm, int i) {
        conn = this.kon.getKoneksi();
        try {
            ps = conn.prepareStatement("select o.id_obat, o.nama_obat, o.harga, ro.jumlah "
                    + "from resep r, resep_obat ro, obat o "
                    + "where r.id_resep=ro.id_resep and ro.id_obat=o.id_obat and r.no_rm=?",
                    ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ps.setString(1, rm);
            rs = ps.executeQuery();
            rs.absolute(i);
            id_Obat = rs.getString(1);
            nama_Obat = rs.getString(2);
            harga = rs.getInt(3);
            banyak = rs.getInt(4);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage() + "  error detailObat");
        } finally {
            try {
                conn.close();
                ps.close();
                rs.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }
    }
}
