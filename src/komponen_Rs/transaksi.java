package komponen_Rs;

public class transaksi {

    private String idTransaksi;
    private Pasien pasien;
    private biaya[] jenis_Biaya;
    private int banyak_Biaya, x = 0;
    private double sum = 0;
    private String bayar;
    

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public String getBayar() {
        return bayar;
    }

    public void setBayar(String bayar) {
        this.bayar = bayar;
    }

    public transaksi(int banyak_Biaya) {
        this.jenis_Biaya = jenis_Biaya;
        jenis_Biaya = new biaya[banyak_Biaya];
    }

    public String getIdTransaksi() {
        return idTransaksi;
    }

    public void setIdTransaksi(String idTransaksi) {
        this.idTransaksi = idTransaksi;
    }

    public Pasien getPasien() {
        return pasien;
    }

    public void setPasien(Pasien pasien) {
        this.pasien = pasien;
    }

    public biaya[] getJenis_Biaya() {
        return jenis_Biaya;
    }

    public void setJenis_Biaya(biaya[] jenis_Biaya) {
        this.jenis_Biaya = jenis_Biaya;
    }

    public int getBanyak_Biaya() {
        return banyak_Biaya;
    }

    public void setBanyak_Biaya(int banyak_Biaya) {
        this.banyak_Biaya = banyak_Biaya;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void tambahBiaya(biaya jenis_Biaya) {
        this.jenis_Biaya[x] = jenis_Biaya;
        x++;
    }

    public double hitungBiayaTotal() {
        for (int i = 0; i < x; i++) {
            sum += jenis_Biaya[i].getBiaya();
        }
        return sum;
    }

    public String printDetailBiaya() {
        bayar = "";
        for (int i = 0; i < x; i++) {
            if (jenis_Biaya[i] instanceof Dokter) {
                System.out.println("Biaya Dokter\t: " + jenis_Biaya[i].getBiaya());
                bayar = bayar + " Biaya Dokter : " + jenis_Biaya[i].getBiaya() + "\n";
            }
            if (jenis_Biaya[i] instanceof Obat) {
                System.out.println("Biaya Obat " + i + " : " + jenis_Biaya[i].getBiaya());
                bayar = bayar + " Biaya obat : " + i + " " + jenis_Biaya[i].getBiaya() + "\n";
            }
            if (jenis_Biaya[i] instanceof Kelas1) {
                System.out.println("Biaya Kamar kelas 1: " + jenis_Biaya[i].getBiaya());
                bayar = bayar + " Biaya kamar kelas 1 : " + jenis_Biaya[i].getBiaya() + "\n";
            }
            if (jenis_Biaya[i] instanceof Kelas2) {
                System.out.println("Biaya kamar kelas 2: " + jenis_Biaya[i].getBiaya());
                bayar = bayar + " Biaya kamar kelas 2 : " + jenis_Biaya[i].getBiaya() + "\n";
            }
            if (jenis_Biaya[i] instanceof Kelas3) {
                System.out.println("Biaya Kamar kelas 3" + jenis_Biaya[i].getBiaya());
                bayar = bayar + " Biaya kamar kelas 3 : " + jenis_Biaya[i].getBiaya() + "\n";
            }
//            if (jenis_Biaya[i] instanceof Kamar) {
//                System.out.println("Biaya inap : " + jenis_Biaya[i].getBiaya());
//            }
            if (jenis_Biaya[i] instanceof rawatJalan) {
                bayar = bayar + " Biaya rawat jalan : " + jenis_Biaya[i].getBiaya() + "\n";
            }
        }
        return bayar;
    }
    
    public void resetJenisBiaya() {
        x=0;
        sum=0;
        for (int i = 0; i < x; i++) {
            jenis_Biaya[i]=null;
        }
    }
}
