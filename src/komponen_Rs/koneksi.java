
package komponen_Rs;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class koneksi {

    public Connection getKoneksi() {
        String host = "172.23.9.185";
        String port = "1521";
        String db = "orcl";
        String user = "MHS185314114";
        String password = "MHS185314114";
        Connection conn = null;

        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        try {
            conn = DriverManager.getConnection("jdbc:oracle:thin:@" + host
                    + ":" + port + ":" + db, user, password);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        if (conn == null) {
            JOptionPane.showMessageDialog(null, "Koneksi gagal");
        }
        return conn;
    }
}
