package komponen_Rs;
public class Kelas1 extends Kamar {
    private final double HARGA_INAP_PERHARI=100000;
    

    public Kelas1() {
    }

    public String getNo_Km() {
        return no_Km;
    }

    public void setNo_Km(String no_Km) {
        this.no_Km = no_Km;
    }

    public String getBagian() {
        return bagian;
    }

    public void setBagian(String bagian) {
        this.bagian = bagian;
    }

    public int getLama() {
        return lama;
    }

    public void setLama(int lama) {
        this.lama = lama;
    }
    
    @Override
    public double totalHarga() {
        return (lama*HARGA_INAP_PERHARI)+(JASAMEDIS*lama);
    }

    @Override
    public double getBiaya() {
        return totalHarga();
    }
      
    
}
