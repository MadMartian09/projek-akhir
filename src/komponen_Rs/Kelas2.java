package komponen_Rs;
public class Kelas2 extends Kamar {
      private final double HARGA_INAP_PERHARI=50000;

    public Kelas2() {
    }

    public String getNo_Km() {
        return no_Km;
    }

    public void setNo_Km(String no_Km) {
        this.no_Km = no_Km;
    }

    public String getBagian() {
        return bagian;
    }

    public void setBagian(String bagian) {
        this.bagian = bagian;
    }

    public int getLama() {
        return lama;
    }

    public void setLama(int lama) {
        this.lama = lama;
    }
      
      @Override
     public double totalHarga() {
         return lama*HARGA_INAP_PERHARI;
     } 
      @Override
    public double getBiaya() {
        return totalHarga();
    }
}
