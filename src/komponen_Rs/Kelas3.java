/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package komponen_Rs;

/**
 *
 * @author ASUS
 */
public class Kelas3 extends Kamar {
     private final double HARGA_INAP_PERHARI=25000;

    public Kelas3() {
    }

    public String getNo_Km() {
        return no_Km;
    }

    public void setNo_Km(String no_Km) {
        this.no_Km = no_Km;
    }

    public String getBagian() {
        return bagian;
    }

    public void setBagian(String bagian) {
        this.bagian = bagian;
    }

    public int getLama() {
        return lama;
    }

    public void setLama(int lama) {
        this.lama = lama;
    }
    
     @Override
    public double totalHarga() {
        return lama*HARGA_INAP_PERHARI;
    }
      @Override
    public double getBiaya() {
        return totalHarga();
    }
}
