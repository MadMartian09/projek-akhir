/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import komponen_Rs.*;

/**
 *
 * @author ASUS
 */
public class Kasir extends javax.swing.JFrame {

    /**
     * Creates new form Kasir
     */
    koneksi kon = new koneksi();
    private Connection conn = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private Statement st = null;
    Obat obat1 = new Obat();
    Obat obat2 = new Obat();
    Obat obat3 = new Obat();
    transaksi bayar = new transaksi(20);
    Dokter dok = new Dokter();
    Pasien pas = new Pasien();

    public Kasir() {
        initComponents();
        showDataPasien();
    }

    public void createObjekObat() {
        if (obat1.getJumObat(rm.getText()) == 1) {
            obat1.getDetailObat(rm.getText(), 1);
            bayar.tambahBiaya(obat1);
        } else if (obat1.getJumObat(rm.getText()) == 2) {
            obat1.getDetailObat(rm.getText(), 1);
            bayar.tambahBiaya(obat1);
            obat2.getDetailObat(rm.getText(), 2);
            bayar.tambahBiaya(obat2);
        } else if (obat1.getJumObat(rm.getText()) == 3) {
            obat1.getDetailObat(rm.getText(), 1);
            bayar.tambahBiaya(obat1);
            obat2.getDetailObat(rm.getText(), 2);
            bayar.tambahBiaya(obat2);
            obat3.getDetailObat(rm.getText(), 3);
            bayar.tambahBiaya(obat3);
        }
    }

    public void createObjekDokter() {
        dok.getDetailDokter(rm.getText());
        bayar.tambahBiaya(dok);
    }

    public void getKelasLama() {
        String kelas = null;
        int lama = 0;
        if (pas.status(rm.getText()).equals("Rawat Inap")) {
            conn = this.kon.getKoneksi();
            try {
                ps = conn.prepareStatement("select round(sysdate-pb.tanggal_masuk), k.kelas "
                        + "from pasien_berobat pb, kamar k "
                        + "where pb.no_rm=? and k.no_rm=?");
                ps.setString(1, rm.getText());
                ps.setString(2, rm.getText());
                rs = ps.executeQuery();
                while (rs.next()) {
                    lama = rs.getInt(1);
                    kelas = rs.getString(2);
                }
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
                System.out.println("Error di kelasLama");
            } finally {
                try {
                    conn.close();
                    ps.close();
                    rs.close();
                } catch (SQLException e) {
                    JOptionPane.showMessageDialog(null, e.getMessage());
                }
            }
            if (kelas.equalsIgnoreCase("Kelas 1")) {
                Kelas1 k1 = new Kelas1();
                k1.setLama(lama);
                bayar.tambahBiaya(k1);
            } else if (kelas.equalsIgnoreCase("Kelas 2")) {
                Kelas2 k2 = new Kelas2();
                k2.setLama(lama);
                bayar.tambahBiaya(k2);
            } else if (kelas.equalsIgnoreCase("Kelas 3")) {
                Kelas3 k3 = new Kelas3();
                k3.setLama(lama);
                bayar.tambahBiaya(k3);
            }
            System.out.println(lama);
            System.out.println(kelas);
        } else if (pas.status(rm.getText()).equals("Rawat Jalan")) {
            rawatJalan rj = new rawatJalan();
            bayar.tambahBiaya(rj);
        }

    }

    public void showDataPasien() {
        DefaultTableModel model = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        TabelPasien.setModel(model);
        model.addColumn("No. Rekam Medis");
        model.addColumn("Nama");
        model.addColumn("Tanggal Lahir");
        model.addColumn("Alamat");
        model.addColumn("Dokter");
        model.addColumn("Status");
        model.addColumn("Tanggal Berobat");
        model.getDataVector().removeAllElements();
        model.fireTableDataChanged();
        conn = this.kon.getKoneksi();
        try {
            st = conn.createStatement();
            String query = "select p.no_RM, p.nama, p.TTL, p.alamat, d.nama, pb.status, pb.tanggal_berobat "
                    + "from pasien p, pasien_berobat pb, "
                    + "dokter d where pb.id_dokter=d.id_dokter and pb.status <> 'Selesai' and pb.no_rm=p.no_rm";
            rs = st.executeQuery(query);

            while (rs.next()) {
                Object[] obj = new Object[7];//array untuk nambah kolom tabel
                obj[0] = (rs.getString(1));
                obj[1] = (rs.getString(2));
                java.sql.Date tanggal = (rs.getDate(3));
                DateFormat d = new SimpleDateFormat("YYYY-MM-dd");
                obj[2] = (d.format(tanggal));
                obj[3] = (rs.getString(4));
                obj[4] = rs.getString(5);
                obj[5] = rs.getString(6);
                obj[6] = rs.getDate(7);
                model.addRow(obj); //menambah baris

            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            try {
                conn.close();
                st.close();
                rs.close();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TabelPasien = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        rm = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        nama = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        nik = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        AlamatArea = new javax.swing.JTextArea();
        jLabel9 = new javax.swing.JLabel();
        JK = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        TglLahir = new javax.swing.JLabel();
        Dokter = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jLabel17 = new javax.swing.JLabel();
        total = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        detilBayar = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setText("Pembayaran Pasien");

        TabelPasien.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        TabelPasien.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TabelPasienMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(TabelPasien);

        jLabel2.setText("No. Rekam Medis :");

        jLabel4.setText("Nama : ");

        jLabel6.setText("NIK : ");

        jLabel8.setText("Alamat : ");

        AlamatArea.setColumns(20);
        AlamatArea.setRows(5);
        jScrollPane2.setViewportView(AlamatArea);

        jLabel9.setText("Jenis Kelamin :");

        jLabel11.setText("Tanggal lahir : ");

        jLabel12.setText("Dokter : ");

        jLabel16.setText("Detail : ");

        jButton1.setText("Bayar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel17.setText("Total bayar : ");

        detilBayar.setEditable(false);
        detilBayar.setColumns(20);
        detilBayar.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        detilBayar.setLineWrap(true);
        detilBayar.setRows(5);
        jScrollPane3.setViewportView(detilBayar);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(rm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(nama, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(nik, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 148, Short.MAX_VALUE)
                            .addComponent(JK, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(TglLahir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(96, 96, 96)
                                .addComponent(jButton1)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(49, 49, 49)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(Dokter, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(jLabel17)
                                                .addGap(18, 18, 18)
                                                .addComponent(total, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(jLabel16)
                                                .addGap(13, 13, 13)
                                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(0, 28, Short.MAX_VALUE))))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(172, 172, 172)))
                .addGap(44, 44, 44))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(138, 138, 138)
                    .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(339, 339, 339)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(Dokter, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel2)
                                    .addComponent(rm, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel12)))))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel4)
                                    .addComponent(nama, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel6)
                                    .addComponent(nik, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(24, 24, 24)
                                .addComponent(jLabel16)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(jScrollPane3)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(total, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(13, 13, 13)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(JK, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel11)
                        .addComponent(TglLahir, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                    .addContainerGap(288, Short.MAX_VALUE)
                    .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(177, 177, 177)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void updatePasien() {
        if (pas.status(rm.getText()).equals("Rawat Inap")) {
            updateInap();
        } else if (pas.status(rm.getText()).equals("Rawat Jalan")) {
            updateJalan();
        }
    }

    private void updateInap() {
        DateFormat dateF = new SimpleDateFormat();
        LocalDateTime now = LocalDateTime.now();
        conn = this.kon.getKoneksi();
        try {
            ps = conn.prepareStatement("update pasien_berobat set tanggal_keluar=?, status=? "
                    + "where no_rm=?");
            ps.setTimestamp(1, new java.sql.Timestamp(System.currentTimeMillis()));
            ps.setString(2, "Selesai");
            ps.setString(3, rm.getText());
            ps.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage() + " Error di updateInap");
        } finally {
            try {
                ps.close();
                conn.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }
    }
    private void TabelPasienMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TabelPasienMouseClicked
        int column = 0;
        int row = TabelPasien.getSelectedRow();
        String value = TabelPasien.getModel().getValueAt(row, column).toString();
        conn = this.kon.getKoneksi();
        try {
            ps = conn.prepareStatement("select p.no_rm, p.nama, p.nik, p.alamat, p.ttl,"
                    + "p.jenis_kelamin, d.nama"
                    + " from pasien p, dokter d, pasien_berobat pb"
                    + " where p.no_rm=? "
                    + "and pb.id_dokter=d.id_dokter and p.no_rm=pb.no_rm");
            ps.setString(1, value);
            rs = ps.executeQuery();
            while (rs.next()) {
                rm.setText(rs.getString(1));
                nama.setText(rs.getString(2));
                nik.setText(rs.getString(3));
                AlamatArea.setText(rs.getString(4));
                Date d = rs.getDate(5);
                DateFormat df = new SimpleDateFormat("dd-MM-YYYY");
                String date = df.format(d);
                TglLahir.setText(date);
                JK.setText(rs.getString(6));
                Dokter.setText(rs.getString(7));

            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());

        } finally {
            try {
                ps.close();
                conn.close();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            }

        }
        createObjekDokter();
        createObjekObat();
        getKelasLama();
        detilBayar.setText(bayar.printDetailBiaya());
        String to = Double.toString(bayar.hitungBiayaTotal());
        total.setText(to);
        bayar.setBayar("");
        bayar.resetJenisBiaya();
    }//GEN-LAST:event_TabelPasienMouseClicked

    private void updateJalan() {
        conn = this.kon.getKoneksi();
        try {
            ps = conn.prepareStatement("update pasien_berobat set status=? "
                    + "where no_rm=?");
            ps.setString(1, "Selesai");
            ps.setString(2, rm.getText());
            ps.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            try {
                ps.close();
                conn.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }
    }
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        DateFormat dateF = new SimpleDateFormat();
        LocalDateTime now = LocalDateTime.now();
        String id_resep = null;
        String id_kamar = null;
        String id_dokter = null;
        String no_rm;
        PreparedStatement ins = null;
        conn = this.kon.getKoneksi();
        try {
            if (pas.status(rm.getText()).equals("Rawat Inap")) {
                ps = conn.prepareStatement("select r.id_resep, k.id_kamar, pb.id_dokter, p.no_rm "
                        + "from resep r, kamar k, pasien p, pasien_berobat pb "
                        + "where r.no_rm=p.no_rm and p.no_rm=k.no_rm and pb.no_rm=p.no_rm "
                        + "and pb.no_rm=k.no_rm "
                        + "and p.no_rm=?");
                ps.setString(1, rm.getText());
                rs = ps.executeQuery();
                while (rs.next()) {
                    id_resep = rs.getString(1);
                    id_kamar = rs.getString(2);
                    id_dokter = rs.getString(3);
                    no_rm = rs.getString(4);
                }
            } else if (pas.status(rm.getText()).equals("Rawat Jalan")) {
                ps = conn.prepareStatement("select r.id_resep, p.id_dokter, p.no_rm "
                        + "from resep r, pasien p "
                        + "where r.no_rm=p.no_rm "
                        + "and p.no_rm=?");
                ps.setString(1, rm.getText());
                rs = ps.executeQuery();
                while (rs.next()) {
                    id_resep = rs.getString(1);
                    id_dokter = rs.getString(2);
                    no_rm = rs.getString(3);
                }
            }
            ins = conn.prepareStatement("insert into transaksi values(?,?,?,?,?,?,?)");
            ins.setString(1, rm.getText() + "TRX");
            ins.setTimestamp(2, new java.sql.Timestamp(System.currentTimeMillis()));
            ins.setString(3, id_resep);
            ins.setString(4, id_kamar);
            ins.setString(5, id_dokter);
            ins.setDouble(6, Double.parseDouble(total.getText()));
            ins.setString(7, rm.getText());

            ins.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage() + " Erorr di update JBUTTON");
        } finally {
            try {
                ps.close();
                rs.close();
                ins.close();
                conn.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
            JOptionPane.showMessageDialog(null, "Data Berhasil Disimpan!");
        }
        updatePasien();
        upDateKamar();
        showDataPasien();
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void upDateKamar() {
        conn = this.kon.getKoneksi();
        try {
            ps = conn.prepareStatement("update kamar set no_rm=? "
                    + "where no_rm=?");
            ps.setString(1, null);
            ps.setString(2, rm.getText());
            ps.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            try {
                ps.close();
                conn.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Kasir.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Kasir.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Kasir.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Kasir.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Kasir().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea AlamatArea;
    private javax.swing.JLabel Dokter;
    private javax.swing.JLabel JK;
    private javax.swing.JTable TabelPasien;
    private javax.swing.JLabel TglLahir;
    private javax.swing.JTextArea detilBayar;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel nama;
    private javax.swing.JLabel nik;
    private javax.swing.JLabel rm;
    private javax.swing.JLabel total;
    // End of variables declaration//GEN-END:variables

}
