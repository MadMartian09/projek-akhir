package GUI;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import komponen_Rs.*;

public class admin extends javax.swing.JFrame {

    Pasien pas = new Pasien();
    koneksi kon = new koneksi();
    Dokter dok = new Dokter();

    String status;

    public admin() {
        initComponents();
        panel_rawat_jalan.show(false);
        rawat_inap.show(false);

//        Tabel_rawat_inap.setModel(model);
        getDataJalan();
        getDataInap();
        addDoc();
    }

   

    public void getDataJalan() {
        DefaultTableModel model = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tabel_rawat_jalan.setModel(model);
        model.addColumn("No. Rekam Medis");
        model.addColumn("Nama");
        model.addColumn("Tanggal Lahir");
        model.addColumn("Alamat");
        model.addColumn("Dokter");
        model.getDataVector().removeAllElements();
        model.fireTableDataChanged();
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        conn = this.kon.getKoneksi();
        try {
            st = conn.createStatement();
            String query = "select p.no_RM, p.nama, p.TTL, p.alamat, d.nama from pasien p, "
                    + "dokter d, pasien_berobat pb"
                    + " where pb.id_dokter=d.id_dokter and pb.status like'Rawat Jalan' and pb.no_rm=p.no_rm "
                    + "and pb.status <> 'Selesai'";
            rs = st.executeQuery(query);

            while (rs.next()) {
                Object[] obj = new Object[5];//array untuk nambah kolom tabel
                obj[0] = (rs.getString(1));
                obj[1] = (rs.getString(2));
                java.sql.Date tanggal = (rs.getDate(3));
                DateFormat d = new SimpleDateFormat("YYYY-MM-dd");
                obj[2] = (d.format(tanggal));
                obj[3] = (rs.getString(4));
                obj[4] = rs.getString(5);
                model.addRow(obj); //menambah baris

            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            try {
                st.close();
                rs.close();
                conn.close();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }
    }

    public void getDataInap() {
        DefaultTableModel model = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;//supaya tabel gabisa diedit
            }
        };
        Tabel_rawat_inap.setModel(model);//mengatur tabel rawat inap agar format seperti objel model
        model.addColumn("No. Rekam Medis");
        model.addColumn("Nama");
        model.addColumn("Tanggal Lahir");
        model.addColumn("Alamat");
        model.addColumn("Dokter");
        model.getDataVector().removeAllElements();//kalo nambah tabel supaya gak dobel, dan dikosongin baru ditambah
        model.fireTableDataChanged();//satu paket
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        conn = this.kon.getKoneksi();
        try {
            st = conn.createStatement();
            String query = "select p.no_RM, p.nama, p.TTL, p.alamat, d.nama from pasien p, "
                    + "dokter d, pasien_berobat pb"
                    + " where pb.id_dokter=d.id_dokter and pb.status like'Rawat Inap' and pb.no_rm=p.no_rm "
                    + "and pb.status <> 'Selesai'";
            rs = st.executeQuery(query);

            while (rs.next()) {
                Object[] obj = new Object[5];
                obj[0] = (rs.getString(1));
                obj[1] = (rs.getString(2));
                java.sql.Date tanggal = (rs.getDate(3));
                DateFormat d = new SimpleDateFormat("YYYY-MM-dd");
                obj[2] = (d.format(tanggal));
                obj[3] = (rs.getString(4));
                obj[4] = rs.getString(5);
                model.addRow(obj);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());//nangkap kesalahan di sql
        } finally { //kalo nge-try gak error masuknya ke finally
            try {
                st.close();
                rs.close();
                conn.close();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        JK = new javax.swing.ButtonGroup();
        Main_Panel = new javax.swing.JPanel();
        butt_rawat_jalan = new javax.swing.JButton();
        Rawat_Jalan3 = new javax.swing.JButton();
        butt_rawat_inap = new javax.swing.JButton();
        panel_rawat_jalan = new javax.swing.JPanel();
        rm = new javax.swing.JLabel();
        rm_field = new javax.swing.JTextField();
        rm1 = new javax.swing.JLabel();
        nama_field = new javax.swing.JTextField();
        rm2 = new javax.swing.JLabel();
        radio_Laki = new javax.swing.JRadioButton();
        radio_Perempuan = new javax.swing.JRadioButton();
        rm3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        alamat_Area = new javax.swing.JTextArea();
        rm4 = new javax.swing.JLabel();
        tanggalLahir = new org.freixas.jcalendar.JCalendarCombo();
        jLabel1 = new javax.swing.JLabel();
        no_Kartu = new javax.swing.JTextField();
        addButton = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        rm5 = new javax.swing.JLabel();
        dokterCombo = new javax.swing.JComboBox<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabel_rawat_jalan = new javax.swing.JTable();
        rawat_inap = new javax.swing.JPanel();
        nama_inap = new javax.swing.JLabel();
        nama_RawatInap = new javax.swing.JTextField();
        nama_inap1 = new javax.swing.JLabel();
        RM_RawatInap = new javax.swing.JTextField();
        nama_inap3 = new javax.swing.JLabel();
        nama_inap4 = new javax.swing.JLabel();
        laki_rawatInap = new javax.swing.JRadioButton();
        perempuan_RawatInap = new javax.swing.JRadioButton();
        nama_inap5 = new javax.swing.JLabel();
        noKartu_RawatInap = new javax.swing.JTextField();
        tglMasuk_RawatInap = new org.freixas.jcalendar.JCalendarCombo();
        nama_inap7 = new javax.swing.JLabel();
        comboKelas = new javax.swing.JComboBox<>();
        nama_inap8 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        alamat_inap = new javax.swing.JTextArea();
        jScrollPane4 = new javax.swing.JScrollPane();
        Tabel_rawat_inap = new javax.swing.JTable();
        nama_inap9 = new javax.swing.JLabel();
        TTL_inap = new org.freixas.jcalendar.JCalendarCombo();
        addRawatInap = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        listKamar = new javax.swing.JList<>();
        nama_inap10 = new javax.swing.JLabel();
        dokterInap = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        Main_Panel.setBackground(new java.awt.Color(77, 5, 232));

        butt_rawat_jalan.setBackground(new java.awt.Color(77, 5, 232));
        butt_rawat_jalan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pict/handicap (1).png"))); // NOI18N
        butt_rawat_jalan.setText("Rawat Jalan");
        butt_rawat_jalan.setOpaque(false);
        butt_rawat_jalan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                butt_rawat_jalanActionPerformed(evt);
            }
        });

        Rawat_Jalan3.setBackground(new java.awt.Color(77, 5, 232));
        Rawat_Jalan3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pict/writing.png"))); // NOI18N
        Rawat_Jalan3.setText("Edit");
        Rawat_Jalan3.setOpaque(false);
        Rawat_Jalan3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Rawat_Jalan3ActionPerformed(evt);
            }
        });

        butt_rawat_inap.setBackground(new java.awt.Color(77, 5, 232));
        butt_rawat_inap.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pict/medical-stretcher.png"))); // NOI18N
        butt_rawat_inap.setText("Rawat Inap");
        butt_rawat_inap.setOpaque(false);
        butt_rawat_inap.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                butt_rawat_inapActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout Main_PanelLayout = new javax.swing.GroupLayout(Main_Panel);
        Main_Panel.setLayout(Main_PanelLayout);
        Main_PanelLayout.setHorizontalGroup(
            Main_PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Main_PanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(Main_PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(butt_rawat_jalan, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Main_PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(Rawat_Jalan3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(butt_rawat_inap, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(1101, 1101, 1101))
        );
        Main_PanelLayout.setVerticalGroup(
            Main_PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Main_PanelLayout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addComponent(butt_rawat_jalan, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(46, 46, 46)
                .addComponent(butt_rawat_inap, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(60, 60, 60)
                .addComponent(Rawat_Jalan3, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panel_rawat_jalan.setBackground(new java.awt.Color(255, 255, 255));
        panel_rawat_jalan.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Pendaftaran Pasien Rawat Jalan", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 0, 24))); // NOI18N

        rm.setText("No. Rekam Medis : ");

        rm1.setText("Nama : ");

        rm2.setText("Dokter :");

        JK.add(radio_Laki);
        radio_Laki.setText("Laki-laki");

        JK.add(radio_Perempuan);
        radio_Perempuan.setText("Perempuan");

        rm3.setText("Jenis Kelamin : ");

        alamat_Area.setColumns(20);
        alamat_Area.setLineWrap(true);
        alamat_Area.setRows(5);
        jScrollPane1.setViewportView(alamat_Area);

        rm4.setText("Tempat & Tanggal lahir : ");

        jLabel1.setText("No. Kartu Identitas : ");

        addButton.setText("Add");
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });

        jButton2.setText("Reset");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        rm5.setText("Alamat :");

        tabel_rawat_jalan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(tabel_rawat_jalan);

        javax.swing.GroupLayout panel_rawat_jalanLayout = new javax.swing.GroupLayout(panel_rawat_jalan);
        panel_rawat_jalan.setLayout(panel_rawat_jalanLayout);
        panel_rawat_jalanLayout.setHorizontalGroup(
            panel_rawat_jalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_rawat_jalanLayout.createSequentialGroup()
                .addGroup(panel_rawat_jalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_rawat_jalanLayout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(panel_rawat_jalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panel_rawat_jalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(rm)
                                .addComponent(rm1, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panel_rawat_jalanLayout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(rm3, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(panel_rawat_jalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panel_rawat_jalanLayout.createSequentialGroup()
                                        .addGap(36, 36, 36)
                                        .addComponent(radio_Laki, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(radio_Perempuan))
                                    .addGroup(panel_rawat_jalanLayout.createSequentialGroup()
                                        .addGap(24, 24, 24)
                                        .addGroup(panel_rawat_jalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(rm_field, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(nama_field, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                            .addGroup(panel_rawat_jalanLayout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(24, 24, 24)
                                .addComponent(no_Kartu, javax.swing.GroupLayout.DEFAULT_SIZE, 195, Short.MAX_VALUE))))
                    .addGroup(panel_rawat_jalanLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(panel_rawat_jalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(panel_rawat_jalanLayout.createSequentialGroup()
                                .addComponent(addButton, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(35, 35, 35)
                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panel_rawat_jalanLayout.createSequentialGroup()
                                .addGroup(panel_rawat_jalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_rawat_jalanLayout.createSequentialGroup()
                                        .addComponent(rm4)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_rawat_jalanLayout.createSequentialGroup()
                                        .addComponent(rm2, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(66, 66, 66)))
                                .addGroup(panel_rawat_jalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(tanggalLahir, javax.swing.GroupLayout.DEFAULT_SIZE, 183, Short.MAX_VALUE)
                                    .addComponent(dokterCombo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 293, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29))
            .addGroup(panel_rawat_jalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panel_rawat_jalanLayout.createSequentialGroup()
                    .addGap(28, 28, 28)
                    .addComponent(rm5, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(1023, Short.MAX_VALUE)))
        );
        panel_rawat_jalanLayout.setVerticalGroup(
            panel_rawat_jalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_rawat_jalanLayout.createSequentialGroup()
                .addGroup(panel_rawat_jalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_rawat_jalanLayout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(panel_rawat_jalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(rm, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(rm_field, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panel_rawat_jalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(rm1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nama_field, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(panel_rawat_jalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(no_Kartu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(22, 22, 22)
                        .addGroup(panel_rawat_jalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(radio_Laki)
                            .addComponent(radio_Perempuan)
                            .addComponent(rm3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(panel_rawat_jalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(rm4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tanggalLahir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(37, 37, 37)
                        .addGroup(panel_rawat_jalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(rm2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dokterCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29))
                    .addGroup(panel_rawat_jalanLayout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(panel_rawat_jalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(addButton, javax.swing.GroupLayout.DEFAULT_SIZE, 77, Short.MAX_VALUE)
                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(panel_rawat_jalanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_rawat_jalanLayout.createSequentialGroup()
                    .addContainerGap(328, Short.MAX_VALUE)
                    .addComponent(rm5, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(190, 190, 190)))
        );

        rawat_inap.setBackground(new java.awt.Color(255, 255, 255));
        rawat_inap.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Pendaftaran Pasien Rawat Inap", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 0, 24))); // NOI18N

        nama_inap.setText("Nama :");

        nama_inap1.setText("No. Rekam Medis :");

        nama_inap3.setText("No. Kartu Identitas : ");

        nama_inap4.setText("Jenis Kelamin");

        laki_rawatInap.setBackground(new java.awt.Color(255, 255, 255));
        JK.add(laki_rawatInap);
        laki_rawatInap.setText("Laki-Laki");

        perempuan_RawatInap.setBackground(new java.awt.Color(255, 255, 255));
        JK.add(perempuan_RawatInap);
        perempuan_RawatInap.setText("Perepmpuan");

        nama_inap5.setText("Tempat & Tanggal Lahir :");

        nama_inap7.setText("Kelas : ");

        comboKelas.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Kelas 1", "Kelas 2", "Kelas 3" }));
        comboKelas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboKelasActionPerformed(evt);
            }
        });

        nama_inap8.setText("Alamat : ");

        alamat_inap.setColumns(20);
        alamat_inap.setLineWrap(true);
        alamat_inap.setRows(5);
        jScrollPane3.setViewportView(alamat_inap);

        Tabel_rawat_inap.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(Tabel_rawat_inap);

        nama_inap9.setText("Tanggal Masuk : ");

        addRawatInap.setText("Add");
        addRawatInap.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addRawatInapActionPerformed(evt);
            }
        });

        jLabel2.setText("No. Kamar : ");

        jScrollPane5.setViewportView(listKamar);

        nama_inap10.setText("Dokter :");

        javax.swing.GroupLayout rawat_inapLayout = new javax.swing.GroupLayout(rawat_inap);
        rawat_inap.setLayout(rawat_inapLayout);
        rawat_inapLayout.setHorizontalGroup(
            rawat_inapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(rawat_inapLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(rawat_inapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(rawat_inapLayout.createSequentialGroup()
                        .addGroup(rawat_inapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nama_inap8, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(52, 52, 52)
                        .addGroup(rawat_inapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane3)
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                    .addComponent(nama_inap3, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(rawat_inapLayout.createSequentialGroup()
                        .addGroup(rawat_inapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nama_inap4, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nama_inap, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nama_inap1, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nama_inap5, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nama_inap9, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(35, 35, 35)
                        .addGroup(rawat_inapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tglMasuk_RawatInap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(TTL_inap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(rawat_inapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(nama_RawatInap, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(RM_RawatInap, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(noKartu_RawatInap, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(rawat_inapLayout.createSequentialGroup()
                                .addComponent(laki_rawatInap)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(perempuan_RawatInap))))
                    .addGroup(rawat_inapLayout.createSequentialGroup()
                        .addGroup(rawat_inapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nama_inap7, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nama_inap10, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(52, 52, 52)
                        .addGroup(rawat_inapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(comboKelas, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(dokterInap, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 281, Short.MAX_VALUE)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20))
            .addGroup(rawat_inapLayout.createSequentialGroup()
                .addGap(123, 123, 123)
                .addComponent(addRawatInap, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        rawat_inapLayout.setVerticalGroup(
            rawat_inapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(rawat_inapLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(rawat_inapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(rawat_inapLayout.createSequentialGroup()
                        .addGroup(rawat_inapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(RM_RawatInap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nama_inap1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(rawat_inapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nama_inap)
                            .addComponent(nama_RawatInap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(15, 15, 15)
                        .addGroup(rawat_inapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nama_inap3)
                            .addComponent(noKartu_RawatInap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(rawat_inapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nama_inap4)
                            .addComponent(laki_rawatInap)
                            .addComponent(perempuan_RawatInap))
                        .addGap(25, 25, 25)
                        .addGroup(rawat_inapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nama_inap5, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(TTL_inap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(rawat_inapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tglMasuk_RawatInap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nama_inap9))
                        .addGap(21, 21, 21)
                        .addGroup(rawat_inapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nama_inap7)
                            .addComponent(comboKelas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(rawat_inapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nama_inap10)
                            .addComponent(dokterInap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(rawat_inapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, rawat_inapLayout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(17, 17, 17)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
                        .addGroup(rawat_inapLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, rawat_inapLayout.createSequentialGroup()
                                .addComponent(nama_inap8)
                                .addGap(46, 46, 46))))
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(addRawatInap, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(Main_Panel, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rawat_inap, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addGap(0, 145, Short.MAX_VALUE)
                    .addComponent(panel_rawat_jalan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Main_Panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(rawat_inap, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(panel_rawat_jalan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void butt_rawat_jalanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_butt_rawat_jalanActionPerformed
        panel_rawat_jalan.setVisible(true);
        rawat_inap.setVisible(false);
        status = "Rawat Jalan";
    }//GEN-LAST:event_butt_rawat_jalanActionPerformed

    private void butt_rawat_inapActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_butt_rawat_inapActionPerformed
        rawat_inap.setVisible(true);
        panel_rawat_jalan.setVisible(false);
        status = "Rawat Inap";

    }//GEN-LAST:event_butt_rawat_inapActionPerformed

    public void addDoc() {
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        conn = this.kon.getKoneksi();
        int length = 0;
        String[] namaDok = new String[5];
        try {
            st = conn.createStatement();
            String query = "select nama from dokter";
            rs = st.executeQuery(query);
            while (rs.next()) {
                namaDok[length] = rs.getString(1);//
                length++;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        } finally {
            for (int i = 0; i < length; i++) {
                dokterCombo.addItem(namaDok[i]);
                dokterInap.addItem(namaDok[i]);
            }
            try {
                st.close();
                rs.close();
                conn.close();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }
    }
    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
        Date tanggal = tanggalLahir.getDate();
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd");
        String jk = null;
        String ttl = sdf.format(tanggal);
        String nama = nama_field.getText();
        String no_Rm = rm_field.getText();
        String nik = no_Kartu.getText();
        String alamat = alamat_Area.getText();
        pas.setNama(nama);
        pas.setNo_Rm(no_Rm);
        pas.setNik(nik);
        pas.setTanggalLahir(ttl);
        pas.setAlamat(alamat);
        pas.setStatus(status);
        if (radio_Laki.isSelected()) {
            jk = "Laki-laki";
        } else if (radio_Perempuan.isSelected()) {
            jk = "Perempuan";
        }
        pas.setJenis_Kelamin(jk);
        String name = dokterCombo.getSelectedItem().toString();
        pas.setDok(getDokter(name));
        pas.inputData();
        getDataJalan();
        JOptionPane.showMessageDialog(null, "Data Berhasil Disimpan!");
    }//GEN-LAST:event_addButtonActionPerformed


    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        rm_field.setText("");
        nama_field.setText("");
        no_Kartu.setText("");
        alamat_Area.setText("");
        radio_Laki.setSelected(false);
        radio_Perempuan.setSelected(false);
        JK.clearSelection();
        dokterCombo.setSelectedIndex(-1);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void addRawatInapActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addRawatInapActionPerformed
        Date tanggalLahir = TTL_inap.getDate();
        Date tanggalMasuk = tglMasuk_RawatInap.getDate();
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd");
        String jk = null;
        String ttl = sdf.format(tanggalLahir);
        String tglMasuk = sdf.format(tanggalMasuk);
        String nama = nama_RawatInap.getText();
        String noRm = RM_RawatInap.getText();
        String nik = noKartu_RawatInap.getText();
        String jkInap = null;
        if (laki_rawatInap.isSelected()) {
            jkInap = "Laki-laki";
        } else if (perempuan_RawatInap.isSelected()) {
            jkInap = "Perempuan";
        }
        String alamatInap = alamat_inap.getText();
        String noKamar = listKamar.getSelectedValue();
        String kelas = comboKelas.getSelectedItem().toString();
        pas.setNama(nama);
        pas.setNo_Rm(noRm);
        pas.setNik(nik);
        pas.setTanggalLahir(ttl);
        pas.setTanggalMasuk(tglMasuk);
        pas.setJenis_Kelamin(jkInap);
        pas.setAlamat(alamatInap);
        pas.setStatus(status);
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        conn = this.kon.getKoneksi();
        PreparedStatement ps = null;

        try {
            ps = conn.prepareStatement("select id_kamar from kamar where kelas=?"
                    + "and no_kamar=?");
            ps.setString(1, kelas);
            ps.setString(2, noKamar);
            rs = ps.executeQuery();
            while (rs.next()) {
                pas.setId_kamar(rs.getString(1));

            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        } finally {
            try {
                ps.close();
                rs.close();
                conn.close();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            }
            JOptionPane.showMessageDialog(null, "Data Berhasil Disimpan!");
        }

        String dokterNama = dokterInap.getSelectedItem().toString();
        pas.setDok(getDokter(dokterNama));
        pas.inputDataInap();
        pas.updateKamar(pas.getId_kamar(), pas.getNo_Rm());
        getDataInap();
    }//GEN-LAST:event_addRawatInapActionPerformed
    public Dokter getDokter(String nama) {
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        conn = this.kon.getKoneksi();
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("select id_dokter, nama from dokter where nama=?");
            ps.setString(1, nama);
            rs = ps.executeQuery();
            while (rs.next()) {
                dok.setId(rs.getString(1));
                dok.setNama_Dok(rs.getString(2));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
            JOptionPane.showMessageDialog(null, "Error getDok");
        } finally {
            try {
                ps.close();
                rs.close();
                conn.close();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());

            }

            return dok;
        }
    }
    private void comboKelasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboKelasActionPerformed
        String kelas = null;
        if (comboKelas.getSelectedItem().equals("Kelas 1")) {
            kelas = "Kelas 1";
        } else if (comboKelas.getSelectedItem().equals("Kelas 2")) {
            kelas = "Kelas 2";
        } else if (comboKelas.getSelectedItem().equals("Kelas 3")) {
            kelas = "Kelas 3";
        }
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        conn = this.kon.getKoneksi();
        PreparedStatement ps = null;
        String[] noKamar = new String[15];
        int length = 0;
        try {
            ps = conn.prepareStatement("select no_kamar from kamar where kelas=?"
                    + "and no_rm is null");
            ps.setString(1, kelas);
            rs = ps.executeQuery();
            while (rs.next()) {
                noKamar[length] = rs.getString(1);
                length++;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        } finally {
            listKamar.setListData(noKamar);
            try {
                ps.close();
                rs.close();
                conn.close();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            }
        }

    }//GEN-LAST:event_comboKelasActionPerformed

    private void Rawat_Jalan3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Rawat_Jalan3ActionPerformed
        Edit e = new Edit();
        e.show(true);
    }//GEN-LAST:event_Rawat_Jalan3ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(admin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(admin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(admin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(admin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {

                new admin().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup JK;
    private javax.swing.JPanel Main_Panel;
    private javax.swing.JTextField RM_RawatInap;
    private javax.swing.JButton Rawat_Jalan3;
    private org.freixas.jcalendar.JCalendarCombo TTL_inap;
    private javax.swing.JTable Tabel_rawat_inap;
    private javax.swing.JButton addButton;
    private javax.swing.JButton addRawatInap;
    private javax.swing.JTextArea alamat_Area;
    private javax.swing.JTextArea alamat_inap;
    private javax.swing.JButton butt_rawat_inap;
    private javax.swing.JButton butt_rawat_jalan;
    private javax.swing.JComboBox<String> comboKelas;
    private javax.swing.JComboBox<String> dokterCombo;
    private javax.swing.JComboBox<String> dokterInap;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JRadioButton laki_rawatInap;
    private javax.swing.JList<String> listKamar;
    private javax.swing.JTextField nama_RawatInap;
    private javax.swing.JTextField nama_field;
    private javax.swing.JLabel nama_inap;
    private javax.swing.JLabel nama_inap1;
    private javax.swing.JLabel nama_inap10;
    private javax.swing.JLabel nama_inap3;
    private javax.swing.JLabel nama_inap4;
    private javax.swing.JLabel nama_inap5;
    private javax.swing.JLabel nama_inap7;
    private javax.swing.JLabel nama_inap8;
    private javax.swing.JLabel nama_inap9;
    private javax.swing.JTextField noKartu_RawatInap;
    private javax.swing.JTextField no_Kartu;
    private static javax.swing.JPanel panel_rawat_jalan;
    private javax.swing.JRadioButton perempuan_RawatInap;
    private javax.swing.JRadioButton radio_Laki;
    private javax.swing.JRadioButton radio_Perempuan;
    private static javax.swing.JPanel rawat_inap;
    private javax.swing.JLabel rm;
    private javax.swing.JLabel rm1;
    private javax.swing.JLabel rm2;
    private javax.swing.JLabel rm3;
    private javax.swing.JLabel rm4;
    private javax.swing.JLabel rm5;
    private javax.swing.JTextField rm_field;
    private javax.swing.JTable tabel_rawat_jalan;
    private org.freixas.jcalendar.JCalendarCombo tanggalLahir;
    private org.freixas.jcalendar.JCalendarCombo tglMasuk_RawatInap;
    // End of variables declaration//GEN-END:variables
}
